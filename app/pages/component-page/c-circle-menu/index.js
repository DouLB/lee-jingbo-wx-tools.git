// app/pages/component-page/c-circle-menu/index.js
const { MenuModel } = require("../../../../dist/models/menuModel")
Page({

    /**
     * 页面的初始数据
     */
    data: {
        menuList: null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getMenuList();
    },

    getMenuList() {
        let uploadFile = new MenuModel({
            iconName: "upgrade",
            bgColor: "#5fbeffab",
            name: "上传",
            action: function () {console.log("uploadFile")},
            flag: "upload",
        }) // 管理员 客服 司机 临时司机
        let library = new MenuModel({
            iconName: "video-o",
            bgColor: "#5fbeffab",
            name: "资源库",
            action: function () {console.log("library")},
            flag: "library",
        })
        let edit = new MenuModel({
            iconName: "edit",
            bgColor: "#5fbeffab",
            name: '编辑',
            action: function () {console.log("edit")},
            flag: "edit",
        }) // 管理员 客服 司机
        let remark = new MenuModel({
            iconName: "records",
            bgColor: "#5fbeffab",
            name: "备注",
            action: function() {console.log("remark")},
            flag: "remark",
        }) // 管理员 客服 司机
        let statement = new MenuModel({
            iconName: "balance-list-o",
            bgColor: "#5fbeffab",
            name: "结算",
            action: function() {console.log("statement")},
            flag: "statement",
        })
        let extra = new MenuModel({
            iconName: "gold-coin-o",
            bgColor: "#5fbeffab",
            name: "补价",
            action: function() {console.log("extra")},
            flag: "extra",
        })
        let print = new MenuModel({
            iconName: "label-o",
            bgColor: "#5fbeffab",
            name: "打印",
            action: function() {console.log("print")},
            flag: "print",
        })
    
        let arrival = new MenuModel({
            iconName: "completed",
            bgColor: "#f4a124ab",
            name: "到达",
            action: function () {console.log("arrival")},
            flag: "arrival",
        }) // 管理员 客服 司机 
        let send = new MenuModel({
            iconName: "logistics",
            bgColor: "#f4a124ab",
            name: "出发",
            action: function () {console.log("send")},
            flag: "send",
        }) // 管理员 客服 司机 
        let alloction = new MenuModel({
            iconName: "cluster-o",
            bgColor: "#f4a124ab",
            name: "派单",
            action: function() {console.log("allocation")},
            flag: "allocation",
        })
    
        let confirmSigned = new MenuModel({
            iconName: 'passed',
            bgColor: "#ff2525ab",
            name: "代签收",
            action: function () {console.log("confirmSigned")},
            flag: "confirmSigned",
        }) // 所有人除了收件人
        let signed = new MenuModel({
            iconName: "certificate",
            bgColor: "#ff2525ab",
            name: "签收",
            action: function () {console.log("signed")},
            flag: "signed",
        }) // 收件人
        let deleteOrder = new MenuModel({
            iconName: "delete-o",
            bgColor: "#ff2525ab",
            name: "删除",
            action: function () {console.log("deleteOrder")},
            flag: "delete",
        }) // 管理员 客服
    
        let normalShare = new MenuModel({
            iconName: "wechat",
            bgColor: "#5eb670ab",
            iconColor: "#04BE02",
            name: "普通分享",
            action: function() {console.log("normalShare")},
            flag: "normal_share",
        })
        let tempDriverShare = new MenuModel({
            iconName: "wechat",
            bgColor: "#5eb670ab",
            iconColor: "#04BE02",
            name: "临时司机",
            action: function() {console.log("tempDriverShare")},
            flag: "temp_driver_share",
        })
        let stationShare = new MenuModel({
            iconName: "wechat",
            bgColor: "#5eb670ab",
            iconColor: "#04BE02",
            name: "临时站点",
            action: function() {console.log("stationShare")},
            flag: "station_share",
        })
        let share = new MenuModel({
            iconName: "share-o",
            bgColor: "#5eb670ab",
            name: "分享",
            action: function () {console.log("share")},
            flag: "share",
            childList: [normalShare, tempDriverShare, stationShare],
            customChildAction: true,
        }) // 所有人
        let share2 = new MenuModel({
            iconName: "share-o",
            bgColor: "#5eb670ab",
            name: "分享2",
            action: function () {console.log("share2")},
            flag: "share2",
            childList: [normalShare, tempDriverShare, stationShare],
            customChildAction: false,
        }) // 所有人
        let menuList = [uploadFile, library, arrival, send, statement, extra, print, alloction, share, share2, remark, signed, confirmSigned, edit, deleteOrder]
        this.setData({
            menuList
        })
    },

    tapMenu(e) {
        console.log("tapMenu", e.detail.flag);
        switch(e.detail.flag) {
            case "share":
                console.log("share-1")
                return;
            default:
                return
        }
    }
})