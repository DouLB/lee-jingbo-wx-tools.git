// app/pages/component-page/c-picker/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        ranges: [[1,2,3,4], ["t1","t2","t3"],[{name: "n1"},{name: "n2"},{name: "n3"}]],
        rangeKeys: [null,null,"name"]
    },

    changePicker(e) {
        console.log(e.detail.value);
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})