const { KEY_NOTIFICATION_LOGIN_VIEW_SHOW } = require("../../../../dist/keys/notification-key")
const { postNotification } = require("../../../../dist/utils/notificationCenter")

// app/pages/component-page/c-login/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },
    tapLogin() {
        postNotification(KEY_NOTIFICATION_LOGIN_VIEW_SHOW, {show: true});
    },
})