import { stringToDate } from "../../../dist/utils/timeUtil"

// app/pages/index/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        list: [
            {
                name: "c-navi",
                path: "/app/pages/component-page/c-navi/index"
            },
            {
                name: "c-page",
                path: "/app/pages/component-page/c-page/index"
            },
            {
                name: "c-list",
                path: "/app/pages/component-page/c-list/index"
            },
            {
                name: "c-circle-menu",
                path: "/app/pages/component-page/c-circle-menu/index"
            },
            {
                name: "c-login",
                path: "/app/pages/component-page/c-login/index"
            },
            {
                name: "c-panel",
                path: "/app/pages/component-page/c-panel/index"
            },
            {
                name: "c-week",
                path: "/app/pages/component-page/c-week/index"
            },
            {
                name: "c-upload",
                path: "/app/pages/component-page/c-upload/index"
            },
            {
                name: "c-picker",
                path: "/app/pages/component-page/c-picker/index"
            },
            {
                name: "c-picker-date",
                path: "/app/pages/component-page/c-picker-date/index"
            }
        ]
    },

    onLoad() {
        
    }

})