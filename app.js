// app.js
const { initLeeWxTools, getToolsOption, getConfig } = require("./dist/index")
const { toolPrintDebug } = require("./dist/utils/printUtil")
App({
  onLaunch() {
    console.log("config-1", getConfig());
    initLeeWxTools()
    toolPrintDebug(getToolsOption())
    console.log("config-2", getConfig());
  },
  globalData: {
    
  }
})
