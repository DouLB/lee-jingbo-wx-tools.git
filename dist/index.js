const { ToolOptionModel } = require("./models/toolOptionsModel");
const { resetOptions, getOption } = require("./options");
const { getConfig } = require("./config/index");

/**
 * 初始化 工具
 * npm构建后，小程序launch时调用
 * @param {ToolOptionModel} options 配置参数
 */
function initLeeWxTools(options) {
    if (!options) options = new ToolOptionModel();
    resetOptions(options);
}

/**
 * 获取工具配置参数对象
 * 用于内部组件或方法获取配置内容，例如baseUrl等
 * @returns globalOptions
 */
function getToolsOption() {
    return getOption();
}

module.exports = {
    ToolOptionModel,

    initLeeWxTools,
    getToolsOption,

    getConfig
}