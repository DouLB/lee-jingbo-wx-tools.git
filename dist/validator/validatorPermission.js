import { ENUM_ROLE, ENUM_ROLE_CUSTOMER, ENUM_ROLE_STATION, ENUM_ROLE_TEMP_STATION } from "../enum/roleEnum";

/**
 * 添加新的权限校验策略，应该在系统初始化时初始化策略
 * @param {string} key 策略名称
 * @param {function} fn 策略执行方法
 */
function addNewValidatorPermissioStrategies(key, fn) {
    validatorStrategies[key] = fn;
}

const validatorStrategies = {
    "isCustomer": isCustomer,
    "isStation": isStation,
    "isVIP": isVIP,
    "isStationManager": isStationManager,
    "isStationService": isStationService,
    "isStationDriver": isStationDriver,
    "isStationManagerOrService": isStationManagerOrService,
    "isCustomerOrTempStation": isCustomerOrTempStation,
}

function compareRole(permissionRoles, userInfo) {
    let comparePermission = false;
    if (!userInfo || !userInfo.currentRole) return false;
    if (!permissionRoles || permissionRoles.length <= 0) {
        return true;
    } else {
        permissionRoles.forEach(role=>{
            if (userInfo.currentRole.role == role.role) {
                if (role.childRole == null || (role.childRole != null && userInfo.currentRole.childRole == role.childRole)) {
                    comparePermission = true
                }
            }
        })
        return comparePermission;
    }
}

function isCustomer(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.CUSTOMER}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isStation(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.STATION}, {role: ENUM_ROLE.TEMP_STATION}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isVIP(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.CUSTOMER, childRole: ENUM_ROLE_CUSTOMER.VIP}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isStationManager(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.STATION, childRole: ENUM_ROLE_STATION.MANAGER}, {role: ENUM_ROLE.TEMP_STATION, childRole: ENUM_ROLE_TEMP_STATION.MANAGER}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isStationService(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.STATION, childRole: ENUM_ROLE_STATION.SERVICE}, {role: ENUM_ROLE.TEMP_STATION, childRole: ENUM_ROLE_TEMP_STATION.SERVICE}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isStationDriver(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.STATION, childRole: ENUM_ROLE_STATION.DRIVER}, {role: ENUM_ROLE.TEMP_STATION, childRole: ENUM_ROLE_TEMP_STATION.DRIVER}];
    return compareRole(permissionRoles, checkData)?null:errMsg;
}

function isStationManagerOrService(checkData, errMsg) {
    let unStationManager = isStationManager(checkData, true);
    let unStationService = isStationService(checkData, true);
    return unStationManager && unStationService ? errMsg:null;
}

function isTempStation(checkData, errMsg) {
    let permissionRoles = [{role: ENUM_ROLE.TEMP_STATION}];
    return compareRole(permissionRoles, checkData)?null: errMsg;
}

function isCustomerOrTempStation(checkData, errMsg) {
    let unCustomer = isCustomer(checkData, true);
    let unTempStation = isTempStation(checkData, true);
    return unCustomer && unTempStation ? errMsg:null;
}

/** =====================校验对象===================== */
let ValidatorPermission = function() {
    this.cache = []; // 校验规则缓存
    this.validatorType = 'or'; // 整体校验规则： or 或（只有一个失败就抛出）| and 且（全部错误就抛出）
}
/**
 * 校验对象添加校验规则
 * @param {any} checkData 要检查的数据
 * @param {string} rule 校验规则---校验策略名 或者 校验策略名:校验限制参数（以：进行分割）
 * @param {string} errMsg 校验错误信息
 * @param {object} context 校验上下文，默认为null
 */
ValidatorPermission.prototype.add = function(checkData, rule, errMsg, context=null) {
    let array = rule.split(":");
    this.cache.push(function(){ // 使用空函数包装校验方法，并放入缓存
        var strategyKey = array.shift(); // 获取校验规则策略名称
        array.unshift(checkData); // 在首位插入要校验的数据
        array.push(errMsg); // 在末尾插入错误信息
        return validatorStrategies[strategyKey].apply(context, array)
    })
}

ValidatorPermission.prototype.start = function(){
    // 循环检验规则缓存
    let errMsgList = [];
    for(let i = 0; i < this.cache.length; i++ ) {
        let errMsg = this.cache[i](); // 开始执行缓存中的校验方法
        if (errMsg) { // 如果有返回值，校验没有通过， 抛出错误信息
            errMsgList.push(errMsg);
            if (this.validatorType == 'or') return errMsg;
        }
    }
    if (this.validatorType == 'and') {
        if (errMsgList && errMsgList.length == this.cache.length) { // 错误信息缓存长度 不等于 校验缓存长度， 有通过校验的内容
            return errMsgList[0];
        } else {
            return null;
        }
    }
}

/**
 * 设置整体校验规则
 * @param {string} type // or 或（只有一个失败就抛出）and 且（全部错误就抛出）
 */
ValidatorPermission.prototype.setValidatorType = function(type) {
    if (type != 'or' && type != 'and') {
        type = "or"
    }
    this.validatorType = type;
}

module.exports = {
    addNewValidatorPermissioStrategies, // 新增权限校验策略
    ValidatorPermission, // 权限校验对象
}