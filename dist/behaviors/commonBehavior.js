const { removeUserInfoChangeWatch, readUserInfoAsync } = require("../services/userServices");
const { ValidatorPermission } = require("../validator/validatorPermission");

module.exports = Behavior({
  properties: {
    componentsFlag: {
      type: String,
      value: "",
    }, // 组件标识
  },
  data: {
    checkPermissionStrategy: "", // 检查组件权限策略 查看 validatorPermission.js 中的策略
    checkPermissionRuleValue: "", // 检查组件权限规则 限制数值， string类型，以 ：分隔多个参数
    checkPermission: false, // 是否检查当前组件权限
    havePermission: false, // 当前用户是否有当前组件查看权限 用于页面决定组件是否显示
    showItOnUnlogin: false, // 当未登录时展示
    pageHeight: 0, // 页面高度
    pageWidth: 0, // 页面宽度
    userInfo: null, // 用户信息
  },
  ready() {
    this.checkPermission();
    this.getPageHeightAndWidth();
  },
  detached() {
    removeUserInfoChangeWatch(this);
  },
  methods: {
    // 组件通用trigger方法
    getTriggerEvent(name, eventData) {
      this.triggerEvent(name, {
        dataset: {
          ...this.dataset
        },
        ...eventData
      })
    },
    // 组件需要监听使用userInfo, 自主实现该方法
    watchUserInfoChange(userInfo) {

    },
    getPageHeightAndWidth() {
      this.setData({
        pageHeight: wx.getSystemInfoSync().windowHeight,
        pageWidth: wx.getSystemInfoSync().windowWidth
      })
    },
    checkPermission() {
      // 是否检查权限
      if (this.data.checkPermission) {
        // 监听并 读取用户信息
        // 因为使用了notificationCenter监听， 一个组件如果使用两次该方法， 会造成老的监听被新的监听覆盖， 如果页面实现中需要监听userInfo 变动，1，直接使用 this.data.userInfo 获取 behavior 中的数据变动响应， 2. observer 监听 userInfo 变动 , 3 rewrite behavior 中定义的 watchUserInfoChange 方法，在watchUserInfoChange中实现对 用户信息变动的监听
        readUserInfoAsync((info) => {
          this.setData({
            userInfo: info.userInfo
          })
          if (info.userInfo) {
            if (this.data.checkPermissionStrategy) { // 是否有校验规则
              let validatorPermission = new ValidatorPermission();
              let rule = this.data.checkPermissionStrategy;
              // 有校验参数 进行拼接
              if (this.data.checkPermissionRuleValue) rule = `${rule}:${this.data.checkPermissionRuleValue}`;
              // 传入用户对象 校验规则：校验参数 校验失败信息
              validatorPermission.add(info.userInfo, rule, "校验不通过");
              // 开启校验 返回校验结果
              let errMsg = validatorPermission.start();

              if (errMsg) { // 校验不通过
                this.setData({
                  havePermission: false
                })
              } else {
                this.setData({
                  havePermission: true
                })
              }
            } else {
              this.setData({
                havePermission: true
              })
            }
          } else {
            // 当允许未登录时显示，用户信息为空，未登录的用户拥有查看权限
            if (this.data.showItOnUnlogin) {
              this.setData({
                havePermission: true
              })
            } else {
              this.setData({
                havePermission: false
              })
            }
          }
          this.watchUserInfoChange(info.userInfo);
        }, this);
      }
    }
  }
})