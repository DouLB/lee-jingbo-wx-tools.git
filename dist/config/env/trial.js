/**
 * 体验环境配置
 */
 const { getOption } = require("../../options")
 let trial = function(){
    return {
      appName: getOption()? getOption().appName.tri : "", // app名称
      appSlogan: getOption()? getOption().appSlogan.tri : "", // app slogan
      appLogoPath: getOption()? getOption().appLogoPath.tri : "", // appLogo 
      Platfom_Service_Phone: getOption()? getOption().platfomServicePhone.tri : "", // 平台客服电话 
      Base_Url: getOption()? getOption().request.url.tri : "", // 基础路径
      Base_Socket_Url: getOption()? getOption().request.socketUrl.tri : "", // socket 基础路径
      Print_Level: getOption()? getOption().printLevel.tri : "", // 打印级别
      Show_Debug_Page: getOption()? getOption().showDebugPage.tri : "", // 是否显示调试信息页面
    }
 }
 let getTrial = trial
 
 /**
  * 开发环境配置
  */
 module.exports = {getTrial}