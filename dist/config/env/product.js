/**
 * 生产环境配置
 */
 const { getOption } = require("../../options")
 let product = function(){
    return {
      appName: getOption()? getOption().appName.pro : "", // app名称
      appSlogan: getOption()? getOption().appSlogan.pro : "", // app slogan
      appLogoPath: getOption()? getOption().appLogoPath.pro : "", // appLogo 
      Platfom_Service_Phone: getOption()? getOption().platfomServicePhone.pro : "", // 平台客服电话 
      Base_Url: getOption()? getOption().request.url.pro : "", // 基础路径
      Base_Socket_Url: getOption()? getOption().request.socketUrl.pro : "", // socket 基础路径
      Print_Level: getOption()? getOption().printLevel.pro : "", // 打印级别
      Show_Debug_Page: getOption()? getOption().showDebugPage.pro : "", // 是否显示调试信息页面
    }
 }
 let getProduct = product
 
 /**
  * 开发环境配置
  */
 module.exports = {getProduct}