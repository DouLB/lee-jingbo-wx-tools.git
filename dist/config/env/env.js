/**
 * 获取当前环境
 * @param {string} fnType host/envVersion 获取环境方法
 */
export function getEnv(fnType) {
    let fnList = {
        host: getEnvWithHost,
        envVersion: getEnvWithEnvVersion
    };
    return fnList[fnType]();
}

function getEnvWithHost() {
    let systemInfo = wx.getSystemInfoSync();
    if (systemInfo.host) {
        return 'release';
    } else {
        return 'develop';
    }
}

function getEnvWithEnvVersion() {
    const { miniProgram: {envVersion}} = wx.getAccountInfoSync();
    return envVersion;
}