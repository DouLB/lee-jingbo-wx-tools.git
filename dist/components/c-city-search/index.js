import { searchCity } from "../../services/searchCityServices"

// components/c-city-search/index.js
var commonBehavior = require("../../behaviors/commonBehavior")
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        label: {
            type: String,
            value: ""
        }, // 左侧label
        placeholder: {
            type: String,
            value: "搜索城市"
        }, // placeholder
        inputAlign: {
            type: String,
            value: "left"
        }, // 输入textAlign
        componentKey: {
            type: String,
            value: ""
        }, // 组件标识
        cityValue: {
            type: String,
            value: ""
        }, // 外部传入city名称
        disabled: {
            type: Boolean,
            value: false
        }, // 是否禁用
        needSearch: {
            type: Boolean,
            value: false, 
        }, // 是否开启输入框联想搜索，false 输入什么就是什么  true 会找后台查找数据，当获取到唯一确定数据指定，没有获取到，默认当前输入项
        bgColor: {
            type: String,
            value: "#fff"
        }, // 背景色
    },

    /**
     * 组件的初始数据
     */
    data: {
        searchKey: null,
        searchCity: null,
        isInitWithValue: false,
    },

    observers: {
        "cityValue": function(cityValue) {
            if (this.data.needSearch) {
                // 搜索结果为空
                // 或者搜索结果 name 与 cityValue 相同
                // 开始搜索
                if (!this.data.searchCity || cityValue != this.data.searchCity.name) {
                    this.data.searchKey = cityValue
                    this.startSearchCity(false)
                }
            } else {
                if (!this.data.searchKey || cityValue != this.data.searchKey) {
                    this.data.searchKey = cityValue;
                    this.startChangeCity();
                }
            }
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {
        startChangeCity() {
            this.getTriggerEvent("search", {city: {name: this.data.searchKey}, key: this.data.componentKey})
        },
        startSearchCity(isDelay) {
            searchCity(this.data.searchKey, (success, city)=>{
                if (success) {
                    this.setData({
                        cityValue: ""
                    })
                    this.changeSearchCity(city);
                }
            }, isDelay);
        },
        tapSearch(e) {
            if (this.data.needSearch) {
                this.startSearchCity(false);
            } else {
                this.startChangeCity();
            }
        },
        changeSearch(e) {
            this.data.searchKey = e.detail;
            if (this.data.needSearch) {
                this.startSearchCity(true);
            } else {
                this.startChangeCity();
            }
        },
        clearSearch(e) {
            this.data.searchKey = "";
            this.data.searchCity = null;
            if (this.data.needSearch) {
                this.startSearchCity(true);
            } else {
                this.startChangeCity();
            }
        },
        changeSearchCity(city) {
            this.setData({
                searchCity: city
            })
            this.getTriggerEvent("search", {city: this.data.searchCity, key: this.data.componentKey})
        }
    }
})
