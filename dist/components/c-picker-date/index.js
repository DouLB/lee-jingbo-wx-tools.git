import { dateFormat, isLeapYear, stringToDate } from "../../utils/timeUtil";
var commonBehavior = require("../../behaviors/commonBehavior");

const DEFAULT_DATE_FORMAT = "yyyy.MM.dd HH:mm:ss";
const START_YEAR = 1970;
const YEAR_LENGTH = 100;
// dist/components/c-picker-date/index.js
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        useCustomHeader: {
            type: Boolean,
            value: false
        }, // 是否使用自定义header
        datePlaceholder: {
            type: String,
            value: "日期",
        }, // placeholder
        timePlaceholder: {
            type: String,
            value: "时间",
        }, // placeholder
        format: {
            type: String,
            value: null, // 时间格式 yyyy.MM.dd HH:mm:ss, 不支持 h+ 12小时制, 会自动转码成 H+ 24小时制
        }, // picker 类型
        value: {
            type: String,
            value: null
        }, // 日期数据
        startValue: {
            type: String,
            value: null,
        }, // 开始时间
        endValue: {
            type: String,
            value: null,
        }, // 结束时间
    },

    /**
     * 组件的初始数据
     */
    data: {
        height: 0,

        type: "dateTime", // date | time | dateTime
        showDateInput: false, // 展示 dateInput 用于点击展示 picker, 选择日期
        showTimeInput: false, // 展示 timeInput 用于点击展示 picker, 选择时间
        showDatePicker: false, // 展示 datePicker 用于选择日期
        showTimePicker: false, // 展示 timePicker 用于选择时间

        currentFormat: null, // 当前日期格式 默认 "yyyy.MM.dd HH:mm:ss"
        currentValue: null, // 当前选中值 dateString 2022.10.25 16:12:13
        
        dateRange: null, // 日期区间 -- [yearRange(1970-2070), monthRange(1-12), dayRange(year & month决定)]
        timeRange: null, // 时间区间 -- [hourRange(0-23), minRange(0-60), secRange(0-60)]

        dateValue: null, // 选中日期值 2022.10.25
        datePickerValue: [], // 选中列表[yearIndex, monthIndex, dayIndex]

        timeValue: null, // 选中时间值 16:12:13
        timePickerValue: [], // 选中列表[hourIndex, minIndex, secIndex]

        startDate: null, // 开始日期 -- startValue
        startTime: null, // 开始时间
        endDate: null, // 结束日期 -- endValue
        endTime: null, // 结束时间

        formatDateSplitFlag: null, // date 分隔符 -- format -- 不支持多种分隔符同时存在
        formatTimeSplitFlag: null, // time 分隔符 -- format -- 不支持多种分隔符同时存在
    },

    observers: {
        "showDatePicker, showTimePicker": function(showDatePicker, showTimePicker) {
            if (showDatePicker || showTimePicker) {
                this.setData({
                    height: this.data.pageHeight * 0.6
                })
            }
        },
        "format": function(format) {
            if (format) {
                if (/(h+)/.test(format)) {
                    format = format.replace(/(h+)/g,"H");
                }
                this.setData({
                    currentFormat: format
                })
            }
        },
        "currentFormat": function(currentFormat) {
            if (currentFormat) {
                this.resetSplitFlag(currentFormat);
                this.resetTypeFromFormat(currentFormat);
            }
        },
        "value": function(value) {
            this.setData({
                currentValue: value
            })
        },
        "type": function(type) {
            this.resetInputShow(type);
        },
        "datePickerValue, dateRange": function(datePickerValue, dateRange) {
            if (dateRange && datePickerValue && dateRange.length > 0 && datePickerValue.length > 0) {
                let dateValue = "";
                dateRange.forEach((item, index)=>{
                    dateValue = dateValue?`${dateValue}-${item[datePickerValue[index]]}`:`${item[datePickerValue[index]]}`
                })
                dateValue = dateValue.replace("年", "").replace("月", "").replace("日", "").replace(/\s*/g, "");
                let haveDate = this.haveDateFormat();
                if (haveDate) {
                    dateValue = dateFormat(stringToDate(dateValue), this.data.currentFormat.split(" ")[0]);
                    this.setData({
                        dateValue
                    })
                }
            }
        },
        "timePickerValue, timeRange": function(timePickerValue, timeRange) {
            if (timeRange && timePickerValue && timeRange.length > 0 && timePickerValue.length > 0) {
                let timeValue = "";
                timeRange.forEach((item, index)=>{
                    timeValue = timeValue?`${timeValue}:${item[timePickerValue[index]]}`:`${item[timePickerValue[index]]}`
                })
                timeValue = timeValue.replace("时", "").replace("分", "").replace("秒", "").replace(/\s*/g, "");
                let haveDate = this.haveDateFormat();
                let haveTime = this.haveTimeFormat();
                if (haveTime) {
                    let index = haveDate?1:0;
                    timeValue = `${dateFormat(new Date(), "yyyy-MM")} ${timeValue}`
                    timeValue = dateFormat(stringToDate(timeValue), this.data.currentFormat.split(" ")[index]);
                    this.setData({
                        timeValue
                    })
                }
            }
        },
        "currentValue, currentFormat": function(currentValue, currentFormat) {
            if (currentValue && currentFormat) {
                this.handlerValue(currentValue, currentFormat)
            } 
        },
        "startValue, endValue, currentFormat": function(startValue, endValue, currentFormat) {
            if (startValue && currentFormat) {
                let result = this.getDateValueWithFormat(startValue, currentFormat);
                this.setData({
                    startDate: result.date,
                    startTime: result.time
                })
            }
            if (endValue && currentFormat) {
                let result = this.getDateValueWithFormat(endValue, currentFormat);
                this.setData({
                    endDate: result.date,
                    endTime: result.time
                })
            }
        },
    },

    created() {
        if (!this.data.currentFormat) {
            this.setData({
                currentFormat: DEFAULT_DATE_FORMAT
            })
        }
        if (!this.data.currentValue) {
            this.setData({
                currentValue: dateFormat(new Date(), this.data.currentFormat)
            })
        }
    },

    /**
     * 组件的方法列表
     */
    methods: {

        /** =======================数据处理======================= */

        /** 初始化数据区间 */
        initRange() {
            let dateRange = [];
            let timeRange = [];
            let format = this.data.currentFormat;
            if (this.data.showDateInput) {
                let haveYearValue = this.haveFormat("year");
                let haveMonthValue = this.haveFormat("month");
                let haveDayValue = this.haveFormat("day");
                if (haveYearValue) {
                    let yearRange = [];
                    for (let i = START_YEAR; i <= START_YEAR + YEAR_LENGTH; i++) {
                        yearRange.push(`${i} 年`);
                    }
                    dateRange.push(yearRange);
                }
                if (haveMonthValue) {
                    let monthRange = [];
                    for (let i = 1; i <= 12; i++) {
                        monthRange.push(`${i} 月`);
                    }
                    dateRange.push(monthRange);
                }
                if (haveDayValue) {
                    let yearIndex = null;
                    let monthIndex = null;
                    if (haveYearValue) {
                        yearIndex = this.data.datePickerValue[0];
                    }
                    if (haveMonthValue) {
                        monthIndex = this.data.datePickerValue[0];
                        if (haveYearValue) {
                            monthIndex = this.data.datePickerValue[1];
                        }
                    }
                    let year = yearIndex? yearIndex + START_YEAR : parseInt(dateFormat(new Date(), "yyyy"));
                    let month = monthIndex? monthIndex + 1 : 1;
                    let dayRange = this.getDayRange(year, month);
                    dateRange.push(dayRange);
                }
                this.setData({
                    dateRange
                })
            }
            if (this.data.showTimeInput) {
                let haveHourValue = this.haveFormat("hour", format);
                let haveMinValue = this.haveFormat("min", format);
                let haveSecValue = this.haveFormat("sec", format);
                if (haveHourValue) {
                    let hourRange = [];
                    for (let i = 0; i <= 23; i++) {
                        if (i<10) i = `0${i}`;
                        hourRange.push(`${i} 时`);
                    }
                    timeRange.push(hourRange);
                }
                if (haveMinValue) {
                    let minRange = [];
                    for (let i = 0; i <= 59; i++) {
                        if (i<10) i = `0${i}`;
                        minRange.push(`${i} 分`);
                    }
                    timeRange.push(minRange);
                }
                if (haveSecValue) {
                    let secRange = [];
                    for (let i = 0; i <= 59; i++) {
                        if (i<10) i = `0${i}`;
                        secRange.push(`${i} 秒`);
                    }
                    timeRange.push(secRange);
                }
                this.setData({
                    timeRange
                })
            }
        },
        /** 是否有日期格式 */
        haveDateFormat(format) {
            return this.haveFormat("year", format) || this.haveFormat("month", format) || this.haveFormat("day", format);
        },
        /** 是否有时间格式 */
        haveTimeFormat(format) {
            return this.haveFormat("hour", format) || this.haveFormat("min", format) || this.haveFormat("sec", format);
        },
        /** 是否有格式 */
        haveFormat(flag, format) {
            if (!format) {
                format = this.data.currentFormat;
            }
            switch(flag){
                case "year":
                    return /(y+)/.test(format);
                case "month":
                    return /(M+)/.test(format);
                case "day":
                    return /(d+)/.test(format);
                case "hour":
                    return /(h+)/.test(format) || /(H+)/.test(format);
                case "min":
                    return /(m+)/.test(format);
                case "sec":
                    return /(s+)/.test(format);
            }
        },
        /** 获取day取值区间 */
        getDayRange(year, month) {
            const length_1 = 31, length_2 = 30, length_3 = 29, length_4 = 28;
            let dayRange = [];
            let length = length_1;
            if (year || month) {
                switch(month) {
                    case 4,6,9,11:
                        length = length_2;
                        break;
                    case 2:
                        length = isLeapYear(year)?length_3: length_4;
                        break;
                    default:
                        length = length_1;
                        break;
                }
            }
            for(let i = 1; i <= length; i++) {
                dayRange.push(`${i} 日`)
            }
            return dayRange;
        },
        /** 获取分隔符 */
        resetSplitFlag(format) {
            let formatResult = format.split(" ");
            formatResult.forEach((item, index)=>{
                if (this.haveDateFormat(item)) {
                    item = item.replace(/(y+)/g, "");
                    item = item.replace(/(M+)/g, "");
                    item = item.replace(/(d+)/g, "");
                    this.setData({formatDateSplitFlag: item&&item.length>0?item[0]:""});
                }
                if (this.haveTimeFormat(item)) {
                    item = item.replace(/(h+)/g, "");
                    item = item.replace(/(H+)/g, "");
                    item = item.replace(/(m+)/g, "");
                    item = item.replace(/(s+)/g, "");
                    this.setData({formatTimeSplitFlag: item&&item.length>0?item[0]:""});
                }
            })
        },
        /** 根据 type 解析是否展示 date 和 input */
        resetInputShow(type) {
            let showDateInput = false;
            let showTimeInput = false;
            if (type) {
                switch(type) {
                    case "date": 
                        showDateInput = true;
                        break;
                    case "time":
                        showTimeInput = true;
                        break;
                    case "dateTime":
                        showDateInput = true;
                        showTimeInput = true;
                        break;
                }
            }
            this.setData({
                showDateInput,
                showTimeInput
            })
        },
        /** 数据解析处理 */
        getDateValueWithFormat(value, format) {
            let date = null;
            let time = null;
            if ( value && typeof value == 'string' ) {
                value = dateFormat(stringToDate(value), format);
                let formatList = format.split(" ");
                let resultList = value.split(" ");
                formatList.forEach((item, index)=>{
                    if (this.haveDateFormat(item)) date = resultList[index];
                    if (this.haveTimeFormat(item)) time = resultList[index];
                })

            }
            return {date, time};
        },
        /** 解析 value */
        handlerValue(value, format) {
            let result = this.getDateValueWithFormat(value, format);
            let dateValueResult = result.date.split(this.data.formatDateSplitFlag);
            let timeValutResult = result.time.split(this.data.formatTimeSplitFlag);
            let datePickerValue = [];
            let haveYearValue = this.haveFormat("year", format);
            let haveMonthValue = this.haveFormat("month", format);
            let haveDayValue = this.haveFormat("day", format);
            if (haveYearValue) {
                let year = dateValueResult[0];
                datePickerValue.push(parseInt(year) - START_YEAR);
            }
            if (haveMonthValue) {
                let month = dateValueResult[0];
                if (haveYearValue) month = dateValueResult[1];
                datePickerValue.push(parseInt(month) - 1);
            }
            if (haveDayValue) {
                let day = dateValueResult[dateValueResult.length - 1];
                datePickerValue.push(parseInt(day) - 1);
            }
            let timePickerValue = [];
            let haveHourValue = this.haveFormat("hour", format);
            let haveMinValue = this.haveFormat("min", format);
            let haveSecValue = this.haveFormat("sec", format);
            if (haveHourValue) {
                let hour = timeValutResult[0];
                timePickerValue.push(parseInt(hour));
            }
            if (haveMinValue) {
                let min = timeValutResult[0];
                if (haveHourValue) min = timeValutResult[1];
                timePickerValue.push(parseInt(min));
            }
            if (haveSecValue) {
                let sec = timeValutResult[timeValutResult.length - 1];
                timePickerValue.push(parseInt(sec) - 1);
            }
            this.setData({
                datePickerValue,
                timePickerValue
            })
            this.initRange();
        },
        /** 根据 format 解析 type */
        resetTypeFromFormat(format) {
            if (!format) return;
            let list = format.split(" ");
            let result = [];
            list.forEach(item=>{
                if (this.haveDateFormat(item)) result.push("date");
                if (this.haveTimeFormat(item)) result.push("time");
            })
            if (result.length == 2) {
                result = "dateTime";
            } else if (result.length == 1) {
                result = result[0];
            } else {
                throw new Error("c-picker-date format 错误");
            }
            this.setData({
                type: result
            })
        },
        /** dateValue 变动 */
        dateValueChange(value) {
            this.setData({
                datePickerValue: value
            })
            this.valueChange();
        },
        /** timeValue 变动 */
        timeValueChange(value) {
            this.setData({
                timePickerValue: value
            })
            this.valueChange();
        },
        /** 数据变动 对外方法 */
        valueChange() {
            let value = "";
            if (this.data.dateValue) value = `${this.data.dateValue}`;
            if (this.data.timeValue) value = value?`${value} ${this.data.timeValue}`:`${this.data.timeValue}`;
            this.getTriggerEvent("change", {value})
        },

        /** =======================Event事件======================= */

        /** 点击确定 */
        tapConfirm(e) {
            let value = "";
            if (this.data.dateValue) value = `${this.data.dateValue}`;
            if (this.data.timeValue) value = value?`${value} ${this.data.timeValue}`:`${this.data.timeValue}`;
            this.getTriggerEvent("confirm", {value});
            let type = e.currentTarget.dataset.type;
            switch(type) {
                case "date":
                    this.setData({
                        showDatePicker: false
                    })
                    return;
                case "time":
                    this.setData({
                        showTimePicker: false
                    })
                    return;
            }
        },
        /** 点击mask */
        tapMask(e) {
            this.tapCancel(e);
        },
        /** 点击取消 */
        tapCancel(e) {
            let value = "";
            if (this.data.dateValue) value = `${this.data.dateValue}`;
            if (this.data.timeValue) value = value?`${value} ${this.data.timeValue}`:`${this.data.timeValue}`;
            this.getTriggerEvent("cancel", {value});
            let type = e.currentTarget.dataset.type;
            switch(type) {
                case "date":
                    this.setData({
                        showDatePicker: false
                    })
                    return;
                case "time":
                    this.setData({
                        showTimePicker: false
                    })
                    return;
            }
        },
        /** 点击输入框 */
        tapPicker(e) {
            let type = e.currentTarget.dataset.type;
            switch(type) {
                case "date": 
                    this.setData({
                        showDatePicker: true,
                        showTimePicker: false
                    })
                    return;
                case "time":
                    this.setData({
                        showTimePicker: true,
                        showDatePicker: false
                    })
                    return;
            }   
        },
        /** 选择 */
        pickerChange(e) {
            let type = e.currentTarget.dataset.type;
            let value = e.detail.value;
            switch(type) {
                case "date":
                    let haveYearFormat = this.haveFormat("year");
                    let haveMonthFormat = this.haveFormat("month");
                    let haveDayFormat = this.haveFormat("day");
                    if (haveDayFormat) {
                        let yearIndex = null;
                        let monthIndex = null;
                        if (haveYearFormat) {
                            yearIndex = value[0];
                        }
                        if (haveMonthFormat) {
                            monthIndex = value[0];
                            if (haveYearFormat) {
                                monthIndex = value[1];
                            }
                        }
                        
                        let year = yearIndex? yearIndex + START_YEAR : parseInt(dateFormat(new Date(), "yyyy"));
                        let month = monthIndex? monthIndex + 1 : 1;

                        let dayRange = this.getDayRange(year, month);
                        if (value[value.length - 1] > dayRange.length) {
                            value[value.length - 1] = dayRange.length - 1;
                        }
                        this.setData({
                            [`dateRange[${this.data.dateRange.length - 1}]`]: dayRange
                        })
                        this.dateValueChange(value);
                    }
                    break;
                case "time":
                    this.timeValueChange(value);
                    break;
            }
        },
    }
})
