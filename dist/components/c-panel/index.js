// components/c-panel/index.js
var commonBehavior = require("../../behaviors/commonBehavior")
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        headerLeftIcon: {
            type: String,
            value: null
        }, // header 左icon
        headerLeftIconPrefix: {
            type: String,
            value: "van-icon"
        },
        headerLeftIconColor: {
            type: String,
            value: "#ff5844"
        },
        headerRightIcon: {
            type: String,
            value: null
        }, // header 右icon
        headerRightIconPrefix: {
            type: String,
            value: "van-icon"
        },
        headerRightIconColor: {
            type: String,
            value: "#ff5844"
        },
        title: {
            type: String,
            value: ""
        }, // header 标题
        customHeader: {
            type: Boolean,
            value: false
        }, // 是否自定义 header
        customFooter: {
            type: Boolean,
            value: false
        }, // 是否自定义footer
    },

    options: {
        multipleSlots: true, // 开启多slot支持
    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
