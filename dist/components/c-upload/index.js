// dist/components/c-upload/index.js
var commonBehavior = require("../../behaviors/commonBehavior");
const { getOption } = require("../../options");
Component({
    behaviors: [commonBehavior],

    relations: {
        "../c-upload-item": {
            type: "child",
            linked: function(target) {

            },
            linkChanged: function(target) {

            },
            unlinked: function(target) {
                
            }
        }
    },

    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {

    },

    /**
     * 组件的方法列表
     */
    methods: {

    }
})
