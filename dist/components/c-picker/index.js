// dist/components/c-picker/index.js
var commonBehavior = require("../../behaviors/commonBehavior");
Component({
    behaviors: [commonBehavior],
    options: {
        multipleSlots: true
    },
    /**
     * 组件的属性列表
     */
    properties: {
        useCustomHeader: {
            type: Boolean,
            value: false
        }, // 是否使用自定义header
        placeholder: {
            type: String,
            value: "",
        }, // placeholder
        mode: {
            type: String,
            value: "normal", // normal 普通选择器
        }, // picker 类型

        ranges: {
            type: Array,
            value: [], // [[]] 双重数组
        }, // range
        rangeKeys: {
            type: Array,
            value: null, // [""], string 数组
        }, // range key
        selectedIndexs: {
            type: Array,
            value: null
        }, // 选中的value
        contentValue: {
            type: String,
            value: ""
        }, // content 显示内容， 显示优先级高于 selectedIndexs 决定的内容
    },

    externalClasses: ["c-class"],

    observers: {
        "showPicker": function(showPicker) {
            if (showPicker) {
                this.setData({
                    height: this.data.pageHeight * 0.6
                })
                this.initSelectedIndexs();
            }
        },
        "ranges": function(ranges) {
            if (ranges) {
                this.initPickerIndexs();
            }
        },
        "pickerIndexs": function(pickerIndexs) {
            if (pickerIndexs) {
                this.setPickerContent();
            }
        },
        "selectedIndexs": function(selectedIndexs) {
            this.setData({
                pickerIndexs: selectedIndexs
            })
        }
    },

    attached() {
        this.setData({
            showPicker: false
        })
    },

    /**
     * 组件的初始数据
     */
    data: {
        showPicker: true,
        pickerIndexs:null, // 选中index
        height: 0,
        headerContent: "", // 标题
        pickerValue: null, // 选中内容
    },
    /** 
     * 组件的方法列表
     */
    methods: {
        setPickerValue() {
            let result = [];
            this.data.pickerIndexs.forEach((item, index)=>{
                let column = this.data.ranges[index];
                let row = column[item];
                let rangeKey = this.data.rangeKeys? this.data.rangeKeys[index]: null;
                result.push(rangeKey? row[rangeKey]: row);
            })
            this.setData({
                pickerValue: result.join("-")
            })
        },
        setPickerContent() {
            let result = [];
            this.data.pickerIndexs.forEach((item, index)=>{
                let column = this.data.ranges[index];
                let row = column[item];
                let rangeKey = this.data.rangeKeys? this.data.rangeKeys[index]: null;
                result.push(rangeKey? row[rangeKey]: row);
            })
            this.setData({
                headerContent: result.join("-")
            })
        },
        initPickerIndexs() {
            if (!this.data.selectedIndexs || this.data.selectedIndexs.length != this.data.ranges.length) {
                let indexs = [];
                this.data.ranges.forEach(item=>{
                    indexs.push(0)
                })
                this.setData({
                    pickerIndexs: indexs
                })
            }
        },
        initSelectedIndexs() {
            if (!this.data.selectedIndexs || this.data.selectedIndexs.length != this.data.ranges.length) {
                this.data.selectedIndexs = [];
                this.data.ranges.forEach(item=>{
                    this.data.selectedIndexs.push(0)
                })
                this.setData({
                    selectedIndexs: this.data.selectedIndexs
                })
            }
        },
        tapPicker() {
            this.setData({
                showPicker: true
            })
        },
        tapMask() {},
        pickerChange(e) {
            this.setData({
                pickerIndexs: e.detail.value
            })
            this.getTriggerEvent("change", {value: this.data.pickerIndexs})
        },
        tapCancel() {
            this.setData({
                showPicker: false
            })
        },
        tapConfirm() {
            this.getTriggerEvent("confirm", {value: this.data.pickerIndexs})
            this.setData({
                selectedIndexs: this.data.pickerIndexs,
                showPicker: false
            })
            this.setPickerValue();
        },
    }
})
