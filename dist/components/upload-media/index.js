const {
    apiUPLOAD
} = require("../../request/apiRequest");

const {
    getMediaTypeValue,
    getMediaTypeName,
    analyticsMediaType,
} = require("../../utils/util")

// components/upload-media/index.js
var commonBehavior = require("../../behaviors/commonBehavior");
const { getOption } = require("../../options");

Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        uploadUrl: {
            type: String,
            value: getOption().file.sysUploadUrl,
        },
        urlModule: {
            type: String,
            value: getOption().file.sysUploadUrlModule
        },
        sources: {
            type: Array,
            value: []
        }, // 
        sourceType: {
            type: String,
            value: 'all' // all 相机+相册 album 相册 camera 相机
        },
        maxDuration: {
            type: Number,
            value: 20
        }, // 视频时长
        sizeType: {
            type: String,
            value: 'all' // all 所有 original 原图 compressed 压缩
        },
        camera: {
            type: String,
            value: 'back' // back 后相机 font 前相机
        },
        autoUpload: {
            type: Boolean,
            value: true
        }, // 是否自动上传
        colCount: {
            type: Number,
            value: 4
        }, // 列数
        maxCount: {
            type: Number,
            value: 9
        }, // 最大数量
        singleType: {
            type: Boolean,
            value: false
        }, // 是否单media类型 true 时 默认初始化为 all，且 选择 video 时 maxCount 恒定为 1
        singleMediaType: {
            type: String,
            value: null, // 单Media类型，singleType 为 true 时的默认选中类型, image 图片 video 视频
        },
        ableAdd: {
            type: Boolean,
            default: true
        }, // 是否允许新增
        ableDelete: {
            type: Boolean,
            default: true,
        }, // 是否允许删除
        ableSaveVideo: {
            type: Boolean,
            default: false,
        }, // 是否允许缓存视频
        ableSelected: {
            type: Boolean,
            default: false
        }, // 是否允许选中
        flag: {
            type: String,
            value: null
        }, // 标识，用于页面存在多个上传组件
        formData: {
            type: Object,
            value: {}
        }, // formData
        muted: {
            type: Boolean,
            value: true
        }, // 视频播放是否静音
        beforeDeleteFn: {
            type: Function,
            value: null,
        }, // 异步删除前置方法
        ableSelectedFromLibrary: {
            type: Boolean,
            value: false
        }, // 是否允许从资源库选取
        libraryInfo: {
            type: Object,
            value: null
        }, // 资源库附属参数
    },

    observers: {
        "sources": function (sources) {
            this.sourcesToUploadSrcList(sources);
            if (this.data.autoUpload) {
                this.startUpload();
            }
            this.resetRect();
        },
        "colCount": function (colCount) {
            this.resetRect();
        },
        "uploadSrcList": function (uploadSrcList) {
            if (!uploadSrcList || uploadSrcList.length <= 0) {
                this.data.singleSelectType = this.data.singleMediaType ? this.data.singleMediaType : 'all'
            }
        },
        "singleMediaType": function (singleMediaType) {
            if (singleMediaType) {
                if (singleMediaType !== 'image' || singleMediaType !== 'video') singleMediaType = "all"
                this.data.singleSelectType = singleMediaType ? singleMediaType : 'all'
            }
        }
    },
    attached() {
        this.attachedOperate();
    },
    ready() {
        this.readyOperate();
    },
    lifetimes: {
        attached() {
            this.attachedOperate();
        },
        ready() {
            this.readyOperate 
        }, 
    },

    /**
     * 组件的初始数据
     */
    data: {
        uploadSrcList: [],
        uploadItemStyleObj: {
            width: 0,
            height: 0,
            display: 'inline-block'
        },
        isUploading: false,
        singleSelectType: 'all',
        boxHeight: 0,

        getSizeCount: 0, // 获取尺寸计数
    },

    /**
     * 组件的方法列表
     */
    methods: {
        readyOperate() {
            this.resetRect();
        },
        attachedOperate() {
            this.sourcesToUploadSrcList(this.data.sources);
            if (this.data.singleSelectType) {
                this.data.singleSelectType = this.data.singleMediaType?this.data.singleMediaType:"all";
            }
            if (this.data.autoUpload) {
                this.startUpload();
            }
        },
        sourcesToUploadSrcList(sources) {
            if (sources && sources.length > 0) {
                this.setData({
                    uploadSrcList: sources.map(item=>{
                        if (typeof item == 'string') {
                            return {
                                url: item, // media 地址
                                status: 'success', // 状态 unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
                                progress: 0, // 上传进度
                                type: analyticsMediaType(item),
                                serverObj: {imageUrl: item, imageType: getMediaTypeValue(analyticsMediaType(item)),}, // 服务器返回数 据
                            }
                        } else {
                            return item;
                        }
                    })
                })
            } 
        },
        clearList() {
            this.setData({
                uploadSrcList: []
            })
        },
        resetRect() {
            let $this = this;
            const query = this.createSelectorQuery().in(this);
            query.select('.box').boundingClientRect().exec((res) => {
                let data = res[0];
                if (data) {
                    let width = data.width / $this.data.colCount;
                    let height = width * 1.1;
                    let boxHeight = 0;
                    let temp = this.data.uploadSrcList.length;
                    if (this.data.ableAdd) {
                        temp = temp + 1;
                    }
                    boxHeight = Math.ceil(temp / this.data.colCount) * height;
                    $this.setData({
                        getSizeCount: 0,
                        boxHeight,
                        uploadItemStyleObj: {
                            width: `${width}px`,
                            height: `${height}px`,
                            display: 'inline-block'
                        }
                    })
                } else {
                    // 超过次数中止， 提供默认尺寸
                    if (this.data.getSizeCount < 30) {
                        setTimeout(() => {
                            this.data.getSizeCount++;
                            this.resetRect();
                        }, 300);
                    } else {
                        $this.setData({
                            getSizeCount: 0,
                            boxHeight: 0,
                            uploadItemStyleObj: {
                                width: 0,
                                height: 0,
                                display: 'inline-block'
                            }
                        })
                    }
                }
            })
        },
        tapImage(e) {
            let current = e.detail.obj.url;
            let urls = this.data.uploadSrcList.filter(item => {
                if (item.type == 'image') {
                    return item;
                }
            })
            urls = urls.map(item => {
                return item.url;
            })
            wx.previewImage({
                urls,
                current
            })
        },
        tapSelected(e) {
            if (this.data.isUploading) return;
            let index = e.detail.index;
            let obj = e.detail.obj;
            this.setData({
                [`uploadSrcList[${index}]`]: obj
            })
            this.getTrigger("selected", {selectedIndex: index, selectedObj: obj});
        },
        deleteOperate(index) {
            let deleteList = this.data.uploadSrcList.splice(index, 1);
            this.setData({
                uploadSrcList: this.data.uploadSrcList
            })

            this.getTrigger("delete", {
                deleteItem: deleteList[0],
                deleteIndex: index
            });
        },
        tapDelete(e) {
            if (this.data.isUploading) return;
            if (typeof this.data.beforeDeleteFn == 'function') {
                let $this = this;
                let outDeleteFn = function (index, callback) {
                    let tempList = [...$this.data.uploadSrcList];
                    let deleteList = tempList.splice(index, 1);
                    let deleteItem = deleteList[0];
                    $this.data.beforeDeleteFn(deleteItem, callback)
                }
                outDeleteFn(e.detail.index, (success, data) => {
                    if (success) {
                        this.deleteOperate(e.detail.index);
                    }
                })
            } else {
                this.deleteOperate(e.detail.index);
            }
        },
        tapRetry(e) {
            if (this.data.isUploading) return;
            this.uploadOperate(e.detail.index);
        },
        tapAdd() {
            if (this.data.isUploading) return;
            let $this = this;
            let sourceType = ['album', 'camera'];
            if (this.data.sourceType === 'album') {
                sourceType = ['album']
            } else if (this.data.sourceType === 'camera') {
                sourceType = ['camera']
            }
            if (this.data.singleType) {
                if (this.data.singleSelectType === 'image') {
                    this.chooseImage(sourceType);
                } else if (this.data.singleSelectType === 'video') {
                    this.chooseVideo(sourceType);
                } else {
                    wx.showActionSheet({
                        itemList: ['图片', '视频'],
                        success(res) {
                            if (res.tapIndex === 0) {
                                $this.chooseImage(sourceType);
                            } else if (res.tapIndex === 1) {
                                $this.chooseVideo(sourceType);
                            }
                        }
                    })
                }
            } else {
                let itemList = ['图片', '视频'];
                if (this.data.ableSelectedFromLibrary) {
                    itemList.push("从资源库选取");
                }
                wx.showActionSheet({
                    itemList,
                    success(res) {
                        if (res.tapIndex === 0) {
                            $this.chooseImage(sourceType);
                        } else if (res.tapIndex === 1) {
                            $this.chooseVideo(sourceType);
                        } else {
                            if ($this.data.uploadSrcList.length >= $this.data.maxCount) {
                                wx.showToast({
                                    title: '已达最大数量',
                                    icon: 'none'
                                })
                                return;
                            }
                            this.getTrigger("selectedFromLibrary", {libraryInfo: $this.data.libraryInfo, maxCount: $this.data.maxCount - ($this.data.uploadSrcList ? $this.data.uploadSrcList.length : 0)});
                        }
                    }
                })
            }
        },
        chooseImage(sourceType) {
            let $this = this;
            if (this.data.uploadSrcList.length >= this.data.maxCount) {
                wx.showToast({
                    title: '已达最大数量',
                    icon: 'none'
                })
                return;
            }
            let sizeType = ['original', 'compressed'];
            if (this.data.sizeType === 'original') {
                sizeType = ['original']
            } else if (this.data.sizeType === 'compressed') {
                sizeType = ['compressed']
            }
            wx.chooseImage({
                count: this.data.maxCount - (this.data.uploadSrcList ? this.data.uploadSrcList.length : 0),
                sourceType,
                sizeType,
                success(res) {
                    let tempFilePaths = res.tempFilePaths;
                    let tempFiles = res.tempFiles;
                    let list = tempFilePaths.map(item => {
                        return {
                            url: item, // media 地址
                            status: 'unUpload', // 状态 unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
                            progress: 0, // 上传进度
                            type: 'image', // media 类型 image 图片 video 视频
                            serverObj: null, // 服务器返回数据
                        }
                    })
                    $this.data.singleSelectType = 'image'
                    if (!$this.data.uploadSrcList) {
                        $this.data.uploadSrcList = []
                    }
                    $this.setData({
                        uploadSrcList: $this.data.uploadSrcList.concat(list)
                    })
                    $this.startUpload();
                },
                fail(error) {
                    if (error.errMsg != "chooseImage:fail cancel") {
                        wx.showToast({
                            title: '选取图片失败',
                            icon: 'none'
                        })
                    }
                },
                complete(res) {

                }
            })
        },
        chooseVideo(sourceType) {
            if (this.data.uploadSrcList.length >= this.data.maxCount) {
                wx.showToast({
                    title: '已达最大数量',
                    icon: 'none'
                })
                return;
            }
            if (this.data.singleType && this.data.uploadSrcList.length >= 1) {
                wx.showToast({
                    title: '已达最大数量',
                    icon: 'none'
                })
                return;
            }
            let $this = this;
            wx.chooseVideo({
                camera: this.data.camera,
                maxDuration: this.data.maxDuration,
                sourceType,
                success(res) {
                    let tempFilePath = res.tempFilePath;
                    let file = {
                        url: tempFilePath, // media 地址
                        status: 'unUpload', // 状态 unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
                        progress: 0, // 上传进度
                        type: 'video', // media 类型 image 图片 video 视频
                        serverObj: null, // 服务器返回数据
                    }
                    $this.data.singleSelectType = 'video'
                    if (!$this.data.uploadSrcList) {
                        $this.data.uploadSrcList = []
                    }
                    $this.data.uploadSrcList.push(file);
                    $this.setData({
                        uploadSrcList: $this.data.uploadSrcList
                    })
                    $this.startUpload();
                },
                fail(error) {
                    if (error.errMsg != "chooseVideo:fail cancel") {
                        wx.showToast({
                            title: '选取视频失败',
                            icon: 'none'
                        })
                    }
                },
                complete(res) {

                }
            })
        },
        startUpload() {
            if (this.data.isUploading) return;
            let temp = this.data.uploadSrcList.filter(item=>{
                return item.status != 'success'
            })
            if (temp && temp.length > 0) {
                this.uploadOperate(0);
            }
        },
        uploadOperate(index) {
            if (this.data.uploadSrcList.length <= 0) return;
            this.data.isUploading = true;
            this.uploadMedia(index, (oIndex) => {
                if ((oIndex + 1) < this.data.uploadSrcList.length) {
                    this.uploadOperate(oIndex + 1);
                } else {
                    this.data.isUploading = false;
                    this.getTrigger("uploadComplete");
                }
            })
        },
        uploadMedia(index, completeCallback) {
            let file = this.data.uploadSrcList[index];
            if (file.status == 'success' || file.status == 'uploading') {
                if (typeof completeCallback == 'function') {
                    completeCallback(index);
                }
                return;
            }
            let statusKey = `uploadSrcList[${index}].status`;
            this.setData({
                [statusKey]: "uploading"
            })
            let url = this.data.uploadUrl;
            let formData = {
                imageType: getMediaTypeValue(file.type)
            };
            if (this.data.formData && typeof this.data.formData == 'object') {
                formData = {
                    ...formData,
                    ...this.data.formData
                }
            }
            let keys = Object.keys(formData);
            if (keys && keys.length > 0) {
                let list = keys.map(item => {
                    return `${item}=${formData[item]}`;
                })
                if (url.indexOf("?") != -1) {
                    url = `${url}${list.join("&")}`
                } else {
                    url = `${url}?${list.join("&")}`
                }
            }
            const uploadTask = apiUPLOAD({
                url,
                urlModule: this.data.urlModule,
                filePath: file.url,
                name: 'file',
                formData,
                callback: (success, data) => {
                    if (success) {
                        this.setData({
                            [statusKey]: "success"
                        })
                        file.serverObj = data;
                    } else {
                        this.setData({
                            [statusKey]: "error"
                        })
                        file.serverObj = null;
                    }
                },
                complete: () => {
                    if (typeof completeCallback == 'function') {
                        completeCallback(index);
                    }
                }
            })
            let progressKey = `uploadSrcList[${index}].progress`
            uploadTask.onProgressUpdate(res => {
                this.setData({
                    [progressKey]: res.progress
                })
            })
        },

        getTrigger(triggerName, triggerObj) {
            let tempTriggerObj = {
                list: this.data.uploadSrcList,
                mediaType: this.data.singleType ? this.data.singleSelectType : "all",
                flag: this.data.flag
            };
            if (triggerObj) {
                tempTriggerObj = {
                    ...tempTriggerObj,
                    ...triggerObj
                };
            }
            //   this.triggerEvent(triggerName, tempTriggerObj);
            this.getTriggerEvent(triggerName, tempTriggerObj);
        }
    }
})