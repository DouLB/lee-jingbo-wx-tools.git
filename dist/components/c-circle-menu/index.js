const { MenuModel } = require("../../models/menuModel");

const radius_level_1 = 230;
const radius_level_2 = 370;
const radius_level_3 = 510;

const show_more_length = 12;
const max_length = 17;

const width_small = "300rpx";
const width_middle = "450rpx"
const width_big = "600rpx";

const animation_duration = 200;

const commonBehavior = require("../../behaviors/commonBehavior");

// components/c-circle-menu/index.js
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        menuList: {
            type: Array,
            value: null
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        processMenuList: [],
        open: false,
        bgAnimation: null,
        childButtonAnimationList: []
    },

    created() {
        this.processMenuListData(this.data.menuList);
    },

    observers: {
        menuList: function(menuList) {
            this.processMenuListData(menuList);
            this.setData({
                open: true
            })
        },
        open: function(open) {
            this.startAnimation(open);
        },
    },

    /**
     * 组件的方法列表
     */
    methods: {
        setBGAnimation(open) {
            const animationData = wx.createAnimation({
              duration: animation_duration,
              timingFunction: "ease"
            })
            let width = 0;
            if (open) {
                if (!this.data.processMenuList || this.data.processMenuList.length <= 3) {
                    width = width_small;
                } else if (this.data.processMenuList.length > 3 && this.data.processMenuList.length <= 7) {
                    width = width_middle;
                } else {
                    width = width_big;
                }
            }
            let height = width;
            animationData.width(width).height(height).step();
            this.setData({
                bgAnimation: animationData.export()
            })
        },
        setChildAnimation(open) {
            this.data.processMenuList.forEach((item, index)=>{
                let right = 0;
                let bottom = 0;
                if (open) {
                    right = `${item.right}rpx`;
                    bottom = `${item.bottom}rpx`;
                }
                const animation = wx.createAnimation({
                  duration: animation_duration,
                  timingFunction: "ease"
                })
                animation.right(right).bottom(bottom).step();
                item.animation = animation.export();
            })
            this.setData({
                processMenuList: this.data.processMenuList
            })
        },
        startAnimation(open) {
            this.setBGAnimation(open);
            this.setChildAnimation(open);
        },
        intercept() {
            // infoPrint("c-circle-menu 拦截点击事件")
        },
        tapOpen() {
            if (this.data.open) {
                this.closeMenu();
            } else {
                this.openMenu();
            }
        },
        openMenu() {
            if (this.data.open) return;
            this.setData({
                open: true
            })
        },
        closeMenu() {
            if (!this.data.open) return;
            this.setData({
                open: false
            })
        },
        sendMenuTriggerEvent(menu) {
            if (!menu.childList || menu.childList.length <= 0) {
                this.getTriggerEvent("tapMenu", {...menu});
                if (typeof menu.action == 'function') menu.action();
            } else {
                // 对于多子菜单的菜单项，是否响应自主点击事件，如果不响应，拉起actionSheet显示
                if (!menu.customChildAction) { 
                    if (menu.childList.length == 1) { // 子菜单仅一项，直接响应子菜单事件
                        this.getTriggerEvent("tapMenu", {...menu.childList[0]});
                        if (typeof menu.childList[0].action == 'function') menu.childList[0].action();
                    } else { // 多子菜单， 列表显示
                        wx.showActionSheet({
                          itemList: menu.childList.map((item, index)=>{
                              return item.name?item.name: `菜单${index+1}`;
                          }),
                          success: (res)=>{
                              let childMenu = menu.childList[res.tapIndex];
                              this.getTriggerEvent("tapMenu", {...childMenu})
                              if (typeof childMenu.action == 'function') childMenu.action();
                          }
                        })
                    }
                } else {
                    this.getTriggerEvent("tapMenu", {...menu});
                    if (typeof menu.action == 'function') menu.action();
                }
            } 
        },
        tapMenu(e) {
            this.setData({
                open: false
            })
            let menu = this.data.processMenuList[e.currentTarget.dataset.index];
            this.sendMenuTriggerEvent(menu);
        },
        processMenuListData(menuList) {
            let result = [];
            if (!menuList || menuList.length <= 0) {
                result = []
            } else if (menuList.length <= show_more_length) {
                result = menuList;
            } else {
                if (menuList.length > max_length) { // 超过 12位， 抛弃多余的
                    throw new Error(`菜单不应超过${max_length}位`)
                }
                result = menuList.splice(0, show_more_length - 1);
                let childList = [];
                menuList.forEach((item, index)=>{
                    childList.push(item);
                })
                let moreMenu = new MenuModel({
                    iconName: "ellipsis",
                    flag: 'other',
                    childList,
                })
                result.push(moreMenu)
            }
            this.setData({
                processMenuList: result
            })
            this.calculateMenuPostion();
        },
        calculateMenuPostion() { 
            if (!this.data.processMenuList || this.data.processMenuList.length <= 0) return;
            let menuLength = this.data.processMenuList.length;
            let result = null;
            if (menuLength == 1) {
                result = this.calculatePosition1();
            } else if (menuLength < 4) {
                result = this.calculatePosition2();
            } else if (menuLength <= 5) {
                result = this.calculatePosition3();
            } else if (menuLength <= show_more_length) {
                result = this.calculatePosition4();
            } else {
                throw new Error(`处理后的菜单长度不应超过${show_more_length}位`)
            }
            this.setData({
                processMenuList: result
            })
        },
        calculatePosition1() { // size == 1  第一层 45度角展示
            let result = {...this.data.processMenuList[0]};
            return [{...result, ...this.calculateRightAndBottom()}];
        },
        calculatePosition2() { // size > 1 && size <4  第一层 根据个数平分角度 分区比list长度多1
            return this.data.processMenuList.map((item, index) => {
                let correctAngle = 90 / this.data.processMenuList.length / 2;
                let angle = 90 / this.data.processMenuList.length * (index + 1);
                return {
                    ...item,
                    ...this.calculateRightAndBottom(angle, correctAngle)
                }
            })
        },
        calculatePosition3() { // size <= 5 第一层 2个 3个分区， 第二层， 3个 4个分区
            return this.data.processMenuList.map((item, index) => {
                let angle = 0;
                let correctAngle = 0;
                let radius = radius_level_1;
                if (index < 2) {
                    correctAngle = 90 / 2 / 2;
                    angle = 90/2 * (index+1);
                } else {
                    let tempIndex = index - 2;
                    let tempLength = this.data.processMenuList.length - 2
                    correctAngle = 90/tempLength / 2;
                    angle = 90/tempLength * (tempIndex+1);
                    radius = radius_level_2;
                }
                return {
                    ...item,
                    ...this.calculateRightAndBottom(angle, correctAngle, radius)
                }
            })
        },
        calculatePosition4() { // size > 5 && size < 7 第一层
            return this.data.processMenuList.map((item, index) => {
                let angle = 0;
                let radius = radius_level_1;
                let correctAngle = 0;
                if (index < 3) {
                    correctAngle = 90 / 3 / 2;
                    angle = 90/ 3 * (index + 1);
                } else if (index < 7) {
                    let tempIndex = index - 3;
                    let tempLength = 7 - 3;
                    correctAngle = 90 / tempLength / 2;
                    angle = 90/ tempLength * (tempIndex + 1);
                    radius = radius_level_2;
                } else {
                    let tempIndex = index - 7;
                    let tempLength = this.data.processMenuList.length - 7;
                    correctAngle = 90 / tempLength / 2;
                    angle = 90 / tempLength * (tempIndex + 1);
                    radius = radius_level_3;
                }
                return {
                    ...item,
                    ...this.calculateRightAndBottom(angle, correctAngle, radius)
                }
            })
        },
        /**
         * 计算按钮 right 和 bottom
         * @param {number} angle 目标角度
         * @param {number} correctAngle 纠正角度
         * @param {number} radius 半径长度
         */
        calculateRightAndBottom(angle=45, correctAngle=0, radius=radius_level_1) {
            let result = {};
            result.right = Math.sin((angle-correctAngle)* Math.PI / 180) * radius;
            result.bottom = Math.cos((angle-correctAngle)* Math.PI / 180) * radius;
            result.right = result.right - 50; // 归于中心点
            result.bottom = result.bottom - 50;
            return result;
        }
    }
})
