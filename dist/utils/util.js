const { toolPrintError } = require("./printUtil.js");
const { ENUM_MEDIA_TYPE } = require("../enum/sysEnum");

/**
 * 深度合并对象，obj2 往 obj1 合并，交集字段的数据以obj2 为准，非交集字段数据合并处理。
 * @param {object} obj1 目标对象1 类型优先，obj2 类型不对不合并
 * @param {object} obj2 目标对象2 数据优先，相同字段和类型 覆盖 obj1 数据
 */
function combineObject(obj1, obj2) {
    if (!obj1) obj1 = {};
    if (!obj2) obj2 = {}; // 对空数据初始化
    if (typeof obj1 != 'object' || typeof obj2 != 'object') { // 如果 obj1 和 obj2 有一个不属于object 类型，无法合并，抛出错误
        throw new Error('combineObject 方法合并的数据必须是 object 类型');
    }; 
    return combineLoop(obj1, obj2); // 返回合并完成的数据
}

function combineLoop(obj1, obj2) {
    Object.keys(obj2).forEach(keyItem => { // 遍历 obj2 key
        let valueItem1 = obj1[keyItem];
        let valueItem2 = obj2[keyItem];
        if (valueItem1 != undefined && valueItem1 != null) { // valueItem1 存在， 继续匹配合并
            if (typeof valueItem1 == typeof valueItem1) { // obj1[keyItem] 和 obj2[keyItem] 类型不匹配，以obj1为准， 如果不匹配，忽略obj2的数据
                if (typeof valueItem1 == 'object') { // 如果valueItem1 为 object 递归调用解析
                    obj1[keyItem] = combineLoop(valueItem1, valueItem2);
                } else { // 非object 数据，赋值
                    obj1[keyItem] = valueItem2
                }
            }
        } else { // valueItem1 不存在， 直接将obj2数据合并进obj1
            obj1[keyItem] = valueItem2;
        }
    });
    return obj1; // 返回合并完成的数据
}

/**
 * 解析请求路径参数对象
 * @param {string} url 请求路径
 */
function getObjectFromUrl(url) {
	let tempObj = {};
	if (/\?/.test(url)) {
		let paramStr = url.substring(url.indexOf("?")+1);
		let paramArray = paramStr.split("&");
		paramArray.forEach(item => {
			let resultArray = item.split("=");
			tempObj[resultArray[0]] = resultArray[1];
		});
	}
	return tempObj;
}

/** 参数转参数字符串 */
function param2string(params) {
    if (typeof params == 'object') {
        let keys = Object.keys(params);
        if (keys && keys.length > 0) {
            let list = keys.map(item => {
                return `${item}=${params[item]}`;
            })
            return `${list.join("&")}`;
        }
        return "";
    } else {
        throw new Error("Params 类型错误")
    }
}

/**
 * 获取路径 参数字串
 * @param {object} params 参数对象
 */
function urlAppendParam(params) {
    let paramStr = param2string(params);
    if (paramStr) {
        return `?${paramStr}`
    } 
    return "";
}
/** 过滤空值属性 */
function emptyPrototypeFilter(obj) {
    let result = {};
    Object.keys(obj).forEach(item=>{
        if (obj[item] != null && obj[item] != undefined) {
            result[item] = obj[item]
        }
    })
    return result;
}
/**
 * 拨打电话
 * @param {string} phoneNumber 电话号码
 */
function callPhoneNumber(phoneNumber) {
    if (!phoneNumber) return;
    wx.makePhoneCall({
        phoneNumber,
    }).catch((error)=>{
        toolPrintError(error.errMsg);
    })
}

function getMediaTypeName(typeValue) {
    if (typeof typeValue != 'string') typeValue = `${typeValue}`;
    switch(typeValue) {
        case ENUM_MEDIA_TYPE.IMAGE: 
            return "image"
        case ENUM_MEDIA_TYPE.VIDEO:
            return "video"
        default:
            return "unknow"
    }
}

function getMediaTypeValue(typeName) {
    switch(typeName) {
        case "image": 
            return ENUM_MEDIA_TYPE.IMAGE;
        case "video":
            return ENUM_MEDIA_TYPE.VIDEO;
        default:
            return ENUM_MEDIA_TYPE.UNKNOW;
    }
}
function analyticsMediaType(mediaPath) {
    let imageSub = [".jpg", ".png", ".svg", ".webp", ".gif", ".jpeg"];
    let videoSub = [".mp4", ".mov", ".m4v", ".3gp", ".avi", ".m3u8", ".webm"];
    let mediaType = "unknow";
    imageSub.forEach(subItem=>{
        if (mediaPath.indexOf(subItem) != -1) {
            mediaType = "image"
        }
    })
    videoSub.forEach(subItem=>{
        if (mediaPath.indexOf(subItem) != -1) {
            mediaType = "video"
        }
    })
    return mediaType;
}
/** 检查用户授权 */
function checkAuthSetting(settingName, callback) {
    wx.getSetting({
        success: (getSettingRes)=>{
            if (getSettingRes.authSetting[`scope.${settingName}`] == undefined) { // 未接受或拒绝过此权限
                wx.authorize({
                  scope: `scope.${settingName}`,
                  success: (authorizeRes)=>{ // 同意授权
                    if (typeof callback == 'function') callback(true);
                  },
                  fail: (authorizeError)=>{
                      if (typeof callback == 'function') callback(false, `未授权: ${settingName}`)
                  }
                })
            } else if (getSettingRes.authSetting[`scope.${settingName}`] == false) { // 拒绝权限
                openSettingView(settingName, callback);
            } else { // 已授权
                if (typeof callback == 'function') callback(true);
            }
        }
    })
}
/** 打开授权页面 */
function openSettingView(settingName, callback) {
    wx.openSetting({
        success: (res)=>{
            if (res.authSetting[`scope.${settingName}`]) { // 同意授权
                if (typeof callback == 'function') callback(true)
            } else {
                if (typeof callback == 'function') callback(false, `未授权：${settingName}`)
            }
        },
        fail: (error)=>{
            if (typeof callback == 'function') callback(false, 'openSetting 失败')
        }
    })
}

/** 下载视频 */
function downloadVideo(url, fileExtent = "mp4", callback) {
    let fileName = new Date().valueOf();
    let filePath = `${wx.env.USER_DATA_PATH}/${fileName}.${fileExtent}`;
    wx.downloadFile({
      url,
      filePath,
      success: (downloadRes)=>{
          let tempFilePath = downloadRes.filePath;
          wx.saveVideoToPhotosAlbum({
            filePath: tempFilePath,
            success: (saveRes)=>{
                if (typeof callback == 'function') callback(true)
            },
            fail: (saveError)=>{
                if (typeof callback == 'function') callback(false, "保存视频失败")
            },
            complete: ()=>{
                let fileMgr = wx.getFileSystemManager();
                fileMgr.unlink({//删除临时文件
                    filePath,
                })
            }
          })
      },
      fail: (downloadError)=>{
        if (typeof callback == 'function') callback(false, "视频下载失败")
      }
    })
}

function privatePhone(phone, start=3, end=7, replace='****') {
    if (!phone || phone.length <= end) return phone;
    return phone.replace(phone.substring(start,end), replace)
}
function privateName(name) {
    if (!name) return name;
    name = name.replace(" ","");
    return name.substring(0,1);
}
function resetNaviBarStyle(needDelay=true,title='宠宝机票', backgroundColor='#5fbeff', frontColor='#ffffff') {
    let resetFn = function(title, backgroundColor, frontColor) {
        wx.setNavigationBarTitle({
          title,
        })
        wx.setNavigationBarColor({
          backgroundColor,
          frontColor,
        })
    }
    if (needDelay) {
        setTimeout(()=>{
            resetFn(title, backgroundColor, frontColor)
        },300)
    } else {
        resetFn(title, backgroundColor, frontColor)
    }
}

import Toast from "@vant/weapp/toast/toast"
const { ENUM_TOAST_TYPE } = require("../enum/sysEnum");

const DefaultSelector = "#vant-toast";
const DefaultDuration = 2000;
const DefaultForbidClick = false;

/**
 * 展示Toast
 * 调用该方法页面请先在wxml中添加 <vant-toast> 标签，id 默认"#vant-toast", 如使用其他id, selector需注意传值
 * @param {object} param 
 * @param {string} type loading success fail html 默认loading
 * @param {string} selector 自定义选择器 默认 '#vant-toast'
 * @param {object} context 选择器范围 默认当前页面 可以传入this
 * @param {string} message 内容
 * @param {number} duration 市场(ms) 值为0时  toast 不消失 默认 2000 type==loading 为 0
 * @param {boolean} forbidClick 是否禁止点击背景 默认 false type==loading 为 true
 */
function showToast({
    type = ENUM_TOAST_TYPE.LOADING,
    selector = DefaultSelector,
    context,
    message,
    duration = DefaultDuration,
    forbidClick = DefaultForbidClick,
    onClose
}) {
    let toastTypeList = {
        [ENUM_TOAST_TYPE.LOADING]: showLoading,
        [ENUM_TOAST_TYPE.SUCCESS]: showSuccess,
        [ENUM_TOAST_TYPE.FAIL]: showFail,
        [ENUM_TOAST_TYPE.MESSAGE]: showMessage
    }
    if (!onClose) {
        onClose = function () {}
    }
    return toastTypeList[type]({
        selector,
        context,
        message,
        duration,
        forbidClick,
        onClose
    })
}

function showLoading({
    selector,
    context,
    message,
    duration,
    forbidClick,
    onClose
}) {
    duration = 0;
    forbidClick = true;
    return Toast.loading({
        selector,
        context,
        message,
        duration,
        forbidClick,
        onClose
    })
}

function showMessage({
    selector,
    context,
    message,
    duration,
    forbidClick,
    onClose
}) {
    return Toast({
        selector,
        context,
        message,
        duration,
        forbidClick,
        onClose
    })
}

function showSuccess({
    selector,
    context,
    message,
    duration,
    forbidClick,
    onClose
}) {
    return Toast.success({
        selector,
        context,
        message,
        duration,
        forbidClick,
        onClose
    })
}

function showFail({
    selector,
    context,
    message,
    duration,
    forbidClick,
    onClose
}) {
    return Toast.fail({
        selector,
        context,
        message,
        duration,
        forbidClick,
        onClose
    })
}

function showWarn(msg) {
    showToast({
        type: ENUM_TOAST_TYPE.MESSAGE,
        message: msg
    })
}

/** 隐藏Toast */
function hideToast() {
    Toast.clear();
}

module.exports = {
    getObjectFromUrl,
    param2string,
    urlAppendParam,
    emptyPrototypeFilter,
    callPhoneNumber,
    getMediaTypeName,
    getMediaTypeValue,
    analyticsMediaType,
    checkAuthSetting,
    downloadVideo,
    privatePhone,
    privateName,
    resetNaviBarStyle,
    combineObject,
    showToast,
    hideToast,
    showWarn,
} 