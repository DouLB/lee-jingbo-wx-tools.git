const { toolPrintError } = require("./printUtil")
function saveStorage(key, value) {
    try {
        wx.setStorageSync(key, value)
    } catch (error) {
        toolPrintError("save storage 错误", error.errMsg);
    }
}

function readStorage(key) {
    try {
        return wx.getStorageSync(key);
    } catch (error) {
        toolPrintError("read storage 错误", error.errMsg);
    }
}

function removeStorage(key) {
    try {
        return wx.removeStorageSync(key);
    } catch (error) {
        toolPrintError("remove storage 错误", error.errMsg);
    }
}

module.exports = {
    saveStorage,
    readStorage,
    removeStorage
}