import { IconModel } from "./iconModel";

let MenuModel = function(param = {iconName, iconColor, iconSize, iconPrefix, name, action, flag, permissions, orderStatus, payStatus, childList, customChildAction, bgColor, openType}) {
    if (!param) param = {};
    this.icon = new IconModel();
    this.icon.size = param.iconSize?param.iconSize:24; // 菜单 icon 字体
    this.icon.color = param.iconColor?param.iconColor:"#fff"; // 菜单 icon 颜色
    this.icon.prefix = param.iconPrefix?param.iconPrefix: "van-icon"; // 菜单 icon classPrefix
    this.icon.name = param.iconName?param.iconName: ""; // 菜单 icon 名称
    this.bgColor = param.bgColor?param.bgColor: "#c4d0fb"; // 菜单按钮背景颜色
    this.name = param.name?param.name: ""; // 菜单名称
    this.action = param.action?param.action: null; // 菜单 动作
    this.animation = null; // menu 动画
    this.permissions = param.permissions?param.permissions:[]; // 权限组，用于判断是否显示
    this.orderStatus = param.orderStatus?param.orderStatus:[]; // 菜单对应订单状态组
    this.payStatus = param.payStatus?param.payStatus: []; // 菜单对应订单支付状态
    this.flag = param.flag?param.flag:""; // 标记, 当菜单action 不方便处理的时候， 组件可以往外抛出事件， 通过flag 判断
    this.childList = param.childList?param.childList: []; // 子菜单列表
    this.customChildAction = param.customChildAction; // 是否自定义子菜单事件， 如果为true,自动以actionSheet方式展示，反之需要自己实现
    this.openType = param.openType; // 菜单按钮 button 类型， 默认为空
}

module.exports = {
    MenuModel
}