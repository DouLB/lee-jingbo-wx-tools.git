let PageNotifyModel = function(param) {
	if (!param || typeof param != 'object') {
	   param = {}
	}
	param = {
	   ...initDefaultNotify(),
	   ...param
	}
	return param;
}

function initDefaultNotify() {
	return {
		title: "",
		bgColor: "#ff2525",
		autoClose: false,
		fn: null
	}
}

module.exports = {
	PageNotifyModel
}