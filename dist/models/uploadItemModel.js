import { ENUM_MEDIA_TYPE, ENUM_UPLOAD_MEDIA_STATUS } from "../enum/sysEnum";

let UploadServerResultModel = function(param) {
	this.imageUrl = param.imageUrl;
	this.imageType = param.imageType
}

let UploadItemModel = function(param) {
	this.filePath = param.filePath; // 图片未上传前 本地路径
	this.status = param.status?param.status:ENUM_UPLOAD_MEDIA_STATUS.UNUPLOAD // 状态 unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
    this.progress = param.progress?param.progress: 0; // 上传进度
    this.type = param.type?param.type: ENUM_MEDIA_TYPE.UNKNOW; // media 类型 image 图片 video 视频
    this.serverObj = param.serverObj?param.serverObj: null; // 服务器返回数据
}

module.exports = {
	UploadServerResultModel,
	UploadItemModel,
}