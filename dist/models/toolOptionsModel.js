const { ENUM_PRINT_LEVEL } = require("../enum/optionsEnum");
const { combineObject } = require("../utils/util");
const { addNewValidatorPermissioStrategies } = require("../validator/validatorPermission");

/** option 对象， 参数参照 initDefaultOption 函数 */
let ToolOptionModel = function(param) {
   if (!param || typeof param != 'object') {
      param = {}
   }
   let initData= initDefaultOption();
   initData = combineObject(initData, param);
   handlerOptions(initData);
   return initData;
}

/** 根据 options 执行部分初始化工作 */
function handlerOptions(options) {
   // 权限校验
   if (options.premissionValidatorStrategies && options.premissionValidatorStrategies.length > 0) {
      options.premissionValidatorStrategies.forEach(strategies => {
         addNewValidatorPermissioStrategies(strategies.key, strategies.fn)
      })
   }
}

/** option 对象初始化， 初始化所有参数， 并用于和用户自定义参数值合并 */
function initDefaultOption() {
   return {

      /** app 名称 */
      appName: {
         pro: "宠宝机票",
         dev: "宠宝机票-dev",
         tri: "宠宝机票-tri",
      },

      /** app 标语 */
      appSlogan: {
         pro: "宠宝机票，给您更专业的选择",
         dev: "宠宝机票-dev，给您更专业的选择",
         tri: "宠宝机票-tri，给您更专业的选择",
      },

      /** app logo 路径 */
      appLogoPath: {
         pro: "",
         dev: "",
         tri: "",
      },

      /** 平台客服电话相关 */
      platfomServicePhone: {
         pro: "18170078838",
         dev: "15879067924",
         tri: "16607093121"
      },

      /** 版本相关 */
      version: {
         version: "1.0.0", // 版本号 小功能调整、BUG修复，提交上线后修改小版本；新功能添加，提交上线后修改中版本；大变动，提交上线后修改大版本
         buildCode: 1, // 构建号 同一版本，上线前，每次调整提交后修改构建号
         needRefreshVersion: null, // 需要刷新缓存的最低版本号，null 为当前版本
         needRefreshBuildCode: null, // 需要刷新缓存的最低构建号， null 为任意构建号
      },

      /** 是否显示 debug 相应页面 */
      showDebugPage: {
         pro: false, // 正式版
         dev: true, // 开发版
         tri: true, // 测试版
      },

      /** console 相关 */
      printLevel: {
         pro: ENUM_PRINT_LEVEL.ERROR, // 正式版 打印级别
         dev: ENUM_PRINT_LEVEL.DEBUG, // 开发版 打印级别
         tri: ENUM_PRINT_LEVEL.INFO, // 测试版 打印界别
      },

      /** 请求相关 */
      request: {
         url: {
            pro: "", // 正式服
            dev: "", // 开发服
            tri: "", // 测试服
         },
         urlModule:'',  // 默认请求模块名称
         fileUrlModule:'',  // 默认文件上传模块名称
         tokenSafeList:[], // token 安全列表

         socketUrl: {
            pro: "", // socket 正式服
            dev: "", // socket 开发服
            tri: "", // socket 测试服
         }
      },

      /** 登录、注册、用户相关 */
      login: {
         sysLoginUrl: "", // 系统登录路径
         sysLoginUrlModule: "", // 系统登录模块名称
         sysRegisterUrl: "", // 系统注册路径
         sysRegisterUrlModule: "", // 系统注册模块名称
         sysUserInfoRefreshUrl: "", // 用户信息刷新请求路径
         sysUserInfoRefreshUrlModule: "", // 用户信息刷新请求模块名称
         afterLoginSuccess: function(userInfo, callback){
            if (typeof callback == 'function') {
               callback(true, userInfo)
            }
         }, // 登录成功后操作, 用以登录成功后执行其他特定操作或者对用户信息进行重新整理，必传2个参数，接受用户信息，以及回调方法
      },

      /** 文件相关 */
      file: {
         sysUploadUrl: "", // 默认上传路径
         sysUploadUrlModule: "", // 默认上传模块名称
      },

      /** 字典数据相关 */
      dictData: {
         city: {
            url: `/city/like/name`, // 请求路径
            urlModule: `/map`, // 请求模块名称
         } // 城市数据
      },

      /** 组件权限校验策略 */
      // [{key: string, fn: function}]
      premissionValidatorStrategies: []
   }
}

module.exports = {
    ToolOptionModel
}