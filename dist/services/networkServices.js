
const { showPageNotify } = require("./commonNotifyServices");
/** 开始监听网络变化 */
function openNetworkWatch() {
    if (typeof wx.onNetworkStatusChange == 'function') {
        wx.onNetworkStatusChange((res) => {
            if (!res.isConnected || res.networkType == 'none') { // 无连接 或 无网络
                showPageNotify({show: true, title: "当前无网络连接，请自行查看是否打开手机网络或连接WIFI", bgColor: "#ff2525"});
            } else {
                showPageNotify({show: false})
            }
        })
    }
    if (typeof wx.onNetworkWeakChange == 'function') {
        wx.onNetworkWeakChange((res)=>{
            if (res.weakNet) { // 弱网状态
                showPageNotify({show: true, title: "当前网络处于弱网状态，可能会导致请求超时/请求失败", bgColor: "#f4a124"});
            } else {
                showPageNotify({show: false})
            }
        })
    }
}
/** 结束监听网络变化 */
function closeNetworkWatch() {
    if (typeof wx.offNetworkStatusChange == 'function') {
        wx.offNetworkStatusChange((res)=>{
    
        })
    }
    if (typeof wx.offNetworkWeakChange == 'function') {
        wx.offNetworkWeakChange((res)=>{
    
        })
    }
}

module.exports = {
    openNetworkWatch,
    closeNetworkWatch 
}