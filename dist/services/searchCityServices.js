const { getOption } = require("../options");

/**=======================城市搜索============================= */
let searchCityCache = {};
let SearchCityTimeoutId = null;
const SearchDelayTime = 500; // 搜索延时
/**
 * 搜索城市
 * @param {string} key 关键字
 * @param {function} callback 回调
 * @param {boolean} isDelay 是否延时
 */
function searchCity(key, callback, isDelay) {
    if (!key) {
        if (typeof callback == 'function') callback(false, []);
    }
    let cacheData = searchCityCache[key];
    if (cacheData) {
        if (typeof callback == 'function') callback(true, cacheData)
    } else {
        SearchCityTimeoutId = setTimeout(() => {
            startSearchCity(key, callback);
        }, isDelay?SearchDelayTime:0);
    }
}
function startSearchCity(key, callback) {
    apiGET({
        url: getOption().dictData.city.url,
        urlModule: getOption().dictData.city.urlModule,
        data: {
            name: key
        },
        callback: (success, data)=>{
            // 清除搜索定时器
            clearSearchCityTimeout();
            if (success) {
                if (data && data.length == 1) { // 单一结果
                    searchCityCache[key] = data[0];
                    if (typeof callback == 'function') callback(true, data[0]);
                } else { // 多结果 | 无结果
                    if (typeof callback == 'function') callback(false, []);
                }
            } else { // 请求错误
                if (typeof callback == 'function') callback(false, []);
            }
        }
    })
}
function clearSearchCityTimeout() {
    if (SearchCityTimeoutId) {
        clearTimeout(SearchCityTimeoutId);
        SearchCityTimeoutId = null;
    }
}

module.exports = {
    searchCity
}