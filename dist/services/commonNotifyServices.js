const { KEY_NOTIFICATION_PAGE_NOTIFY_SHOW } = require("../keys/notification-key")
const { postNotification } = require("../utils/notificationCenter")

/**
 * 显示 pageNotify
 * @param {boolean} show 
 * @param {PageNotifyModel} pageNotifyModel 页面通知对象
 */
function showPageNotify(show, pageNotifyModel) {
    if (show) {
        if (!pageNotifyModel) {
            throw new Error("showPageNotify show 为 true 时，pageNotifyModel 不能为空")
        }
    }
    postNotification(KEY_NOTIFICATION_PAGE_NOTIFY_SHOW, {show, pageNotifyModel})
}

module.exports = {
    showPageNotify
}
