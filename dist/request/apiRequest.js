import {
    addNormalNotificationObserver,
    postNotification,
    removeNotificationObserver
} from "../utils/notificationCenter";
import {
    getOption
} from "../options";
import {
    toolPrintDebug,
    toolPrintError
} from "../utils/printUtil";
import {
    getConfig
} from "../config/index";
import {
    readStorage
} from "../utils/storageUtil";
import {
    KEY_STORAGE_USERINFO
} from "../keys/storage-key";
import {
    showWarn,
    hideToast
} from "../utils/util";

function apiRequest({
    url,
    urlModule, // 路径模块标识
    data,
    method = "GET",
    header,
    callback,
    complete
}) {
    let originQuery = {
        ...arguments[0]
    };
    let option = {}; // 请求参数
    option = {
        url: `${getConfig().Base_Url}${urlModule?urlModule:getOption().request.urlModule}${url}?t=${new Date().getTime()}`, // 路径
        method // 方法
    }
    if (data) {
        if (typeof data == 'object' && !(data instanceof Array)) { // 对于非 array 的 object 对象， 判断并移除为空的参数
            let dataResult = {};
            Object.keys(data).forEach(key => {
                if (data[key] != null && data[key] != undefined) {
                    dataResult[key] = data[key]
                }
            })
            option.data = dataResult; // 请求数据
        } else { // 其余对象照常插入
            option.data = data;
        }
    }
    let user = readStorage(KEY_STORAGE_USERINFO)
    let token = user ? user.token : null;
    if (!checkTokenSafe(token, url)) {
        if (typeof originQuery.callback == 'function') originQuery.callback(false, null);
        if (typeof originQuery.complete == 'function') originQuery.callback();
        return;
    }
    if (token) {
        option.header = {
            Authorization: `Bearer ${token}`
        }
    }
    option.header = {
        "content-type": "application/json", // contentType
        ...option.header
    }
    if (header) {
        option.header = {
            ...option.header,
            ...header
        }
    } // header
    toolPrintDebug("请求", url, option);
    let requestTask = wx.request({
        ...option,
        success: (res) => {
            successHandler(originQuery, requestTask, res);
        },
        fail: (error) => {
            failHandler(originQuery, requestTask, error)
        },
        complete: () => {
            completeHandler(originQuery, requestTask);
        }
    })
    return requestTask;
}

function apiGET({
    url,
    urlModule,
    data,
    header,
    callback,
    complete
}) {
    return apiRequest({
        url,
        urlModule,
        data,
        header,
        method: 'GET',
        callback,
        complete
    });
}

function apiPOST({
    url,
    urlModule,
    data,
    header,
    callback,
    complete
}) {
    return apiRequest({
        url,
        urlModule,
        data,
        header,
        method: 'POST',
        callback,
        complete
    });
}

function apiPUT({
    url,
    urlModule,
    data,
    header,
    callback,
    complete
}) {
    return apiRequest({
        url,
        urlModule,
        data,
        header,
        method: 'PUT',
        callback,
        complete
    });
}

function apiDELETE({
    url,
    urlModule,
    data,
    header,
    callback,
    complete
}) {
    return apiRequest({
        url,
        urlModule,
        data,
        header,
        method: 'DELETE',
        callback,
        complete
    });
}

function apiUPLOAD({
    url,
    urlModule = '/file',
    method = "UPLOAD",
    filePath,
    name = "file",
    formData = {},
    header,
    callback,
    complete
}) {
    let originQuery = {
        ...arguments[0]
    };
    let option = {}; // 请求参数
    option = {
        url: `${getConfig().Base_Url}${urlModule?urlModule:getOption().request.fileUrlModule}${url}?t=${new Date().getTime()}`, // 路径
    }
    if (filePath) {
        option.filePath = filePath
    }
    if (name) {
        option.name = name
    }
    if (formData) {
        option.formData = formData
    }
    let user = readStorage(KEY_STORAGE_USERINFO)
    let token = user ? user.token : null;
    if (!checkTokenSafe(token, url)) {
        if (typeof originQuery.callback == 'function') originQuery.callback(false, null);
        if (typeof originQuery.complete == 'function') originQuery.callback();
        return;
    }
    if (token) {
        option.header = {
            Authorization: `Bearer ${token}`
        }
    }
    if (header) {
        option.header = {
            ...option.header,
            ...header
        }
    } // header
    let requestTask = wx.uploadFile({
        ...option,
        success: (res) => {
            successHandler(originQuery, requestTask, res);
        },
        fail: (error) => {
            failHandler(originQuery, requestTask, error)
        },
        complete: () => {
            completeHandler(originQuery, requestTask);
        }
    })
    return requestTask;
}

/** 被挂起的请求 */
const hangupRequest = {};

function successHandler(originQuery, requestTask, res) {
    if (res.statusCode != 200) {
        showWarn(`服务器异常,${res.statusCode}`)
        toolPrintError("请求失败", originQuery.url, res.errMsg);
        if (typeof originQuery.callback == 'function') originQuery.callback(false, res.statusCode);
    } else {
        if (typeof res.data == 'string') res.data = JSON.parse(res.data);
        if (res.data.code == 200) {
            toolPrintDebug("请求结果", originQuery.url, res.data.data);
            if (typeof originQuery.callback == 'function') originQuery.callback(true, res.data.data);
        } else if (res.data.code == 401) {
            toolPrintDebug("请求结果", originQuery.url, res.data.msg);
            hideToast();
            postNotification(KEY_NOTIFICATION_LOGINVIEW_SHOW, {
                show: true,
                requestTask
            });
            addNormalNotificationObserver(KEY_NOTIFICATION_LOGIN_SUCCESS, (info) => {
                if (originQuery.method == 'UPLOAD') {
                    apiUPLOAD(originQuery);
                } else {
                    apiRequest(originQuery);
                }
                removeNotificationObserver(KEY_NOTIFICATION_LOGIN_SUCCESS, requestTask);
            }, requestTask);
            if (typeof originQuery.callback == 'function') originQuery.callback(false, res.data.msg);
        } else {
            toolPrintError("请求失败", originQuery.url, res.data.msg);
            showWarn(res.data.msg)
            if (typeof originQuery.callback == 'function') originQuery.callback(false, res.data.msg);
        }
    }
}

function failHandler(originQuery, requestTask, error) {
    toolPrintError("请求--失败", originQuery.url, error.errMsg);
    showWarn("request fail");
    if (typeof originQuery.callback == 'function') originQuery.callback(false, error.errMsg);
}

function completeHandler(originQuery, requestTask) {
    if (typeof originQuery.complete == 'function') originQuery.complete();
}

function checkTokenSafe(token, url) {
    const whiteList = getOption().request.tokenSafeList; // 不需token白名单
    if (token) return true;
    for (let index = 0; index < whiteList.length; index++) {
        if (url.startsWith(whiteList[index])) {
            return true;
        }
    }
    return false;
}

/**
 * request 转 promise 调用方式
 * @param {*} requestFn apiGET/apiPUT/apiPOST/apiDELETE/apiUPLOAD/apiRequest 函数以及支持此类方法相似参数的请求函数，或者 get,post,put,delete,request 字符串（不区分大小写）
 * @param {*} params 参数，参考 apiGET/apiPUT/apiPOST/apiDELETE/apiUPLOAD/apiRequest 参数
 * @returns promise
 * 
 * // ============= example ============ //
 *      request2Promise(apiGET, {
 *          url: `/order/running/detail/1570590978977517570`,
 *          data: {
 *              orderRunningId: "1570590978977517570"
 *          }
 *      }).then(res=>{
 *           console.log("promiseTest", res);
 *      }).catch(err=>{
 *          console.log("promiseTest error", err);
 *      });
 */
function request2Promise(requestFn, params) {
    return new Promise((resolve, reject) => {
        requestFn = get2PromiseRequestFn(requestFn);
        if (!requestFn) {
            showWarn("请求方法错误");
            reject("请求方法错误");
        } else {
            requestFn({
                ...params,
                callback: (success, data) => {
                    if (success) {
                        resolve(data);
                    } else {
                        reject(data);
                    }
                }
            })
        }
    })
}

function get2PromiseRequestFn(requestFn) {
    if (typeof requestFn == 'function') {
        return requestFn;
    }
    if (typeof requestFn == 'string') {
        requestFn = requestFn.toLowerCase();
        switch (requestFn) {
            case 'get':
                return apiGET;
            case 'post':
                return apiPOST;
            case 'put':
                return apiPUT;
            case "delete":
                return apiDELETE;
            case 'upload':
                return apiUPLOAD;
            case 'request':
                return apiRequest;
            default:
                return null;
        }
    }
    return null;
}

module.exports = {
    apiRequest, // 网络数据请求
    apiGET, // GET 请求
    apiPOST, // POST 请求
    apiPUT, // PUT 请求
    apiDELETE, // DELETE 请求
    apiUPLOAD, // UPLOAD 请求
    request2Promise, // request 转 promise 调用方式
}